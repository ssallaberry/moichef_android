package com.appstud.moichef;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appstud.moichef.Model.Product;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stephane on 25/08/2016.
 */
public class Page3Fragment extends PageFragment {

    @BindView(R.id.image3)  ImageView image3;
    @BindView(R.id.ingredients)  LinearLayout ingredientsView;
    @BindView(R.id.titlePage3)  TextView titlePage3;

    public static Page3Fragment newInstance(Product product) {

        Page3Fragment p = new Page3Fragment();
        p.setProduct(product);
        return p;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.page3, container, false);

        ButterKnife.bind(this, view);

        image3.setAlpha((float) 0.3);

        Typeface titleTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/PathwayGothicOne-Regular.ttf");
        titlePage3.setTypeface(titleTypeface);

        displayPictures();
        displayIngredients();

        // declared in PageFragment
        setOrderButton();//defined in PageFragment

        return view;
    }

    public void displayPictures(){
        if (product != null) {
            Glide.with(this).load(product.getIngredientsMobileImgUrl()).into(image3);
        }
    }

    public void displayIngredients(){
        if (ingredientsView!=null && product!=null) {
            String[] ingredients = product.getIngredients();
            if (ingredientsView.getChildCount() > 0 && ingredients.length > 0){
                ingredientsView.removeAllViews();
            }

            Typeface ingredientTypeFace = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/FanwoodText-Italic.ttf");

            for (String ingredient : ingredients) {
                TextView tempTextView = new TextView(this.getActivity());
                tempTextView.setText("- " + ingredient);
                tempTextView.setTextColor(Color.BLACK);
                tempTextView.setGravity(Gravity.CENTER);
                tempTextView.setTextSize(getResources().getDimension(R.dimen.ingredients_text_size));
                tempTextView.setTypeface(ingredientTypeFace);

                ingredientsView.addView(tempTextView);
            }
        }
    }
}

