package com.appstud.moichef;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstud.moichef.Model.Product;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stephane on 25/08/2016.
 */
public class Page1Fragment extends PageFragment {

    @BindView(R.id.image0) ImageView image0;
    @BindView(R.id.image1) ImageView image1;
    @BindView(R.id.titlePage1) TextView titlePage1;

    public static Page1Fragment newInstance(Product product) {

        Page1Fragment p = new Page1Fragment();
        p.setProduct(product);
        return p;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.page1, container, false);

        ButterKnife.bind(this, view);

        titlePage1.setText(product.getEstablishmentName());
        Typeface titleTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/PathwayGothicOne-Regular.ttf");
        titlePage1.setTypeface(titleTypeface);

        setOrderButton();//defined in PageFragment

        displayPictures();

        return view;
    }



    public void displayPictures(){
        if (product != null) {
            Glide.with(this).load(product.getTitleHwUrl()).into(image0);
            Glide.with(this).load(product.getMainImageUrl()).into(image1);
        }
    }
}

