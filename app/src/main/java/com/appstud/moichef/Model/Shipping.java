package com.appstud.moichef.Model;

/**
 * Created by stephane on 18/08/2016.
 */
public class Shipping {
    private String postal_code;
    private Float rate;

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
