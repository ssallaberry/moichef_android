package com.appstud.moichef.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by stephane on 11/08/2016.
 */
public class Product implements Parcelable {

    private String id;
    private String title;
    private String establishmentName;
    private String[] ingredients;
    private String mainImageUrl;
    private String titleHwUrl;
    private String chiefIllustrationUrl;
    private String ingredientsMobileImgUrl;
    private String qtyMin;
    private String pricePp;

    public Product() {
    }

    public String getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(String establishmentName) {
        this.establishmentName = establishmentName;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getTitleHwUrl() {
        return titleHwUrl;
    }

    public void setTitleHwUrl(String titleHwUrl) {
        this.titleHwUrl = titleHwUrl;
    }

    public String getChiefIllustrationUrl() {
        return chiefIllustrationUrl;
    }

    public void setChiefIllustrationUrl(String chiefIllustrationUrl) {
        this.chiefIllustrationUrl = chiefIllustrationUrl;
    }

    public String getIngredientsMobileImgUrl() {
        return ingredientsMobileImgUrl;
    }

    public void setIngredientsMobileImgUrl(String ingredientsMobileImgUrl) {
        this.ingredientsMobileImgUrl = ingredientsMobileImgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(String[] ingredients) {
        this.ingredients = ingredients;
    }

    // make Product parcelable to give it as an extra to order intent
    public Product(Parcel parcel) {

        this.id = parcel.readString();
        this.title = parcel.readString();
        this.establishmentName = parcel.readString();
        this.ingredients = parcel.createStringArray();
        this.mainImageUrl = parcel.readString();
        this.titleHwUrl = parcel.readString();
        this.chiefIllustrationUrl = parcel.readString();
        this.ingredientsMobileImgUrl = parcel.readString();
        this.qtyMin = parcel.readString();
        this.pricePp = parcel.readString();
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(establishmentName);
        parcel.writeStringArray(ingredients);
        parcel.writeString(mainImageUrl);
        parcel.writeString(titleHwUrl);
        parcel.writeString(chiefIllustrationUrl);
        parcel.writeString(ingredientsMobileImgUrl);
        parcel.writeString(qtyMin);
        parcel.writeString(pricePp);
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>(){

        @Override
        public Product createFromParcel(Parcel parcel) {
            return new Product(parcel);
        }

        @Override
        public Product[] newArray(int i) {
            return new Product[0];
        }
    };

    public String getMinQuantity() {
        return qtyMin;
    }

    public void setMinQuantity(String minQuantity) {
        this.qtyMin = minQuantity;
    }

    public String getPricePp() {
        return pricePp;
    }

    public void setPricePp(String pricePp) {
        this.pricePp = pricePp;
    }


}
