package com.appstud.moichef.Model;

/**
 * Created by stephane on 19/08/2016.
 */
public class Collection {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
