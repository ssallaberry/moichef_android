package com.appstud.moichef;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.appstud.moichef.API.MoiChefService;
import com.appstud.moichef.Model.Collection;
import com.appstud.moichef.Model.Product;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.appstud.moichef.API.API.getApiService;

/**
 * Created by stephane on 19/08/2016.
 */
public class SplashActivity extends AppCompatActivity {
    final String CITY = "paris";
    private static MoiChefService moiChefService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Call<Collection> collectionCall = getApiService().collection(CITY);
        collectionCall.enqueue(new Callback<Collection>() {
            @Override
            public void onResponse(Call<Collection> call, Response<Collection> response) {
                getFeaturedProductFromCollection(response.body().getId());
            }

            @Override
            public void onFailure(Call<Collection> call, Throwable t) {
                Log.e("failure", "onFailure: FAIIIIIL");
                Log.e("failure", String.valueOf(t.getMessage()));
            }
        });

    }


    private void getFeaturedProductFromCollection(int collectionId){
        Call<Product> call = getApiService().productFeatured(collectionId);

        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                showProduct(response.body());
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.e("failure", "onFailure: FAIIIIIL");
                Log.e("failure", String.valueOf(t.getMessage()));
            }
        });
    }

    private void showProduct(Product product){

        Intent intent = new Intent(this, ProductActivity.class);
        intent.putExtra("product", product);
        startActivity(intent);
        finish();

    }

}