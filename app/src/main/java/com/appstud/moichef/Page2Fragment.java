package com.appstud.moichef;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstud.moichef.Model.Product;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stephane on 25/08/2016.
 */
public class Page2Fragment extends PageFragment {

    @BindView(R.id.image2)  ImageView image2;
    @BindView(R.id.titlePage2)  TextView titlePage2;

    public static Page2Fragment newInstance(Product product) {

        Page2Fragment p = new Page2Fragment();
        p.setProduct(product);
        return p;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.page2, container, false);
        ButterKnife.bind(this, view);

        displayPictures();

        Typeface titleTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/PathwayGothicOne-Regular.ttf");
        titlePage2.setText(product.getEstablishmentName());
        titlePage2.setTypeface(titleTypeface);

        // declared in PageFragment
        setOrderButton();//defined in PageFragment

        return view;
    }

    public void displayPictures(){
        if (product != null) {
            Glide.with(this).load(product.getChiefIllustrationUrl()).into(image2);
        }
    }
}

