package com.appstud.moichef;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.appstud.moichef.Model.Product;

import butterknife.BindView;

/**
 * Created by stephane on 25/08/2016.
 */
public abstract class PageFragment extends Fragment {

    protected Product product;
    @BindView(R.id.orderButton)  Button orderButton;


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState);

    // When the user clicks the Send button
    public void goToOrder(View view) {
        Intent intent = new Intent(this.getActivity(), OrderActivity.class);
        intent.putExtra("product", product);
        startActivity(intent);
    }

    protected void setOrderButton(){
        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToOrder(view);
            }
        });
    }
}

