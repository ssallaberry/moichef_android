package com.appstud.moichef.API;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by stephane on 29/08/2016.
 */
public class API {
    private static MoiChefService moiChefService;

    public static MoiChefService getApiService(){
        if (moiChefService == null) {
            // Get product data
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);

            moiChefService = new Retrofit.Builder()
                    .baseUrl(MoiChefService.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
                    .create(MoiChefService.class);
        }
        return moiChefService;

    }
}
