package com.appstud.moichef.API;

import com.appstud.moichef.Model.Collection;
import com.appstud.moichef.Model.Product;
import com.appstud.moichef.Model.Shipping;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by stephane on 11/08/2016.
 */
public interface MoiChefService {
    public static final String ENDPOINT = "https://paris.moichef.fr/api/";

    @GET("collections/name/{name}")
    Call<Collection> collection(@Path("name") String name);

    @GET("products/featured/{id}")
    Call<Product> productFeatured(@Path("id") int id);

     @GET("shipping/{postalCode}")
    Call<Shipping> shippingToPostalCode(@Path("postalCode") String postalCode);

}
