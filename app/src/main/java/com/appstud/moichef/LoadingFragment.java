package com.appstud.moichef;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by stephane on 19/08/2016.
 */
public class LoadingFragment extends Fragment {

    public static LoadingFragment newInstance(){
        return new LoadingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.loading_fragment, container, false);

        return view;
    }



}
