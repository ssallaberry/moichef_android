package com.appstud.moichef;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appstud.moichef.Model.Product;
import com.appstud.moichef.Model.Shipping;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.appstud.moichef.API.API.getApiService;

public class OrderActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, NumberPicker.OnValueChangeListener{
    private static final String TAG = "TAG";

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 123;

    private Product product;
    private String postalCode;
    private Float shippingRate;

    @BindView(R.id.order_image1) ImageView image1;
    @BindView(R.id.textImageOrder) TextView textImage;
    @BindView(R.id.quantity) MaterialEditText quantityInput;
    @BindView(R.id.shipping_date) MaterialEditText shippingDate;
    @BindView(R.id.shipping_time) MaterialEditText shippingTime;
    @BindView(R.id.shipping_address) MaterialEditText shippingAddress;
    @BindView(R.id.info_email) MaterialEditText email;
    @BindView(R.id.info_phone) MaterialEditText phone;
    @BindView(R.id.price_total) TextView priceTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        // views initialization
        ButterKnife.bind(this);

        // data initialization
        product= getIntent().getParcelableExtra("product");

        // prevent keyboard from opening
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // update views with product content
        image1.setAlpha((float) 0.3);
        if (image1 != null && product.getMainImageUrl()!=null)
            Glide.with(this).load(product.getMainImageUrl()).into(image1);

        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/FanwoodText-Italic.ttf");
        textImage.setTypeface(font);
        textImage.setText(product.getTitle());

        quantityInput.setText(product.getMinQuantity());
        updatePriceTotal(Integer.parseInt(product.getMinQuantity()));

        // form controls (pickers...)
        quantityInput.setOnClickListener(this); // number picker
        shippingDate.setOnClickListener(this); // date picker
        shippingTime.setOnClickListener(this); // time picker
        shippingAddress.setOnClickListener(this); // google place autocomplete

        // fields validators
        email.addTextChangedListener(new TextValidator(email) {
            @Override public void validate(TextView textView, String text) {
                email.validateWith(new METValidator(getString(R.string.emailInvalid)) {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        return !isEmpty && OrderActivity.isValidEmail(text);
                    }
                });
            }
        });
        phone.addTextChangedListener(new TextValidator(phone) {
            @Override
            public void validate(TextView textView, String text) {
                phone.validateWith(new METValidator(getString(R.string.phoneInvalid)) {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        return !isEmpty && OrderActivity.isValidPhone(text);
                    }
                });
            }
        });

    }

    @Override
    public void onClick(View view) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        switch (view.getId()){
            case R.id.quantity :
                // Create a new instance of DatePickerDialog and show it
                showNumberPicker();
                break;
            case R.id.shipping_date :
                // Create a new instance of DatePickerDialog and show it
                DatePickerDialog dateDialog = new DatePickerDialog(this, this, year, month, day);
                dateDialog.show();
                break;
            case R.id.shipping_time :
                // Create a new instance of DatePickerDialog and show it
                TimePickerDialog timeDialog = new TimePickerDialog(this, this, hour, minute, true);
                timeDialog.show();
                break;
            case R.id.shipping_address :
                // Create a new instance of DatePickerDialog and show it
                findPlace(shippingAddress);
                break;

            default:

        }

    }

    // set shipping address
    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        shippingDate.setText(datePicker.getDayOfMonth()+"/"+datePicker.getMonth()+"/"+datePicker.getYear());
    }

    // set shipping time
    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        shippingTime.setText(timePicker.getCurrentHour()+" : "+timePicker.getCurrentMinute());
    }

    // update total price when quantity changes
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        updatePriceTotal(newVal);
    }

    private void updatePriceTotal (int quantity){
        float priceTotalFloat = quantity * Float.parseFloat(product.getPricePp());
        priceTotal.setText(Float.toString(priceTotalFloat) + "€");
    }

    // display number picker for quantity
    public void showNumberPicker()
    {
        final Dialog d = new Dialog(this);
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.number_picker_dialog);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setValue(Integer.parseInt(String.valueOf(quantityInput.getText())));
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                quantityInput.setText(String.valueOf(np.getValue()));
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();

    }

    // email validator
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // phone number validator
    public final static boolean isValidPhone(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.PHONE.matcher(target).matches();
    }

    // shipping address place autocomplete
    public void findPlace(View view) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }
    // A place has been received; use requestCode to track the request.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                shippingAddress.setText(place.getName());

                Geocoder geocoder = new Geocoder(this, Locale.FRANCE);

                List<Address> locations = null;
                try {
                    locations = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                    if (locations != null && locations.size()>0){
                        Address address = locations.get(0);
                        postalCode = address.getPostalCode();
                        checkShippingToPostalCode(postalCode);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    // when shipping address is set
    private void checkShippingToPostalCode(String postalCode){

        // request to API
        // if response.rate != null, shipping is available
        // else shipping not available

        Call<Shipping> call = getApiService().shippingToPostalCode(postalCode);

        call.enqueue(new Callback<Shipping>() {
            @Override
            public void onResponse(Call<Shipping> call, Response<Shipping> response) {
                shippingRate = response.body().getRate();
                if (shippingRate == null){
                    errorMessageShipping();
                }else{
                 successMessageShipping();
                }
            }

            @Override
            public void onFailure(Call<Shipping> call, Throwable t) {
                Log.e("failure", "onFailure: FAIIIIIL");
                Log.e("failure", String.valueOf(t.getMessage()));

            }
        });
    }

    private void errorMessageShipping(){
        Toast toast = Toast.makeText(this, "Livraison impossible à cette adresse.", Toast.LENGTH_SHORT);
        toast.show();
    }
    private void successMessageShipping(){
        Toast toast = Toast.makeText(this, "Yay! Livraison possible à cette adresse.", Toast.LENGTH_SHORT);
        toast.show();
    }
}
