package com.appstud.moichef;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appstud.moichef.Model.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stephane on 25/08/2016.
 */
public class Page4Fragment extends PageFragment {
    @BindView(R.id.titlePage4)  TextView titlePage4;
    @BindView(R.id.price) TextView price;
    @BindView(R.id.unitPage4) TextView unitPage4;
    @BindView(R.id.preparationTimeTitle) TextView preparationTimeTitle;
    @BindView(R.id.preparationTime) TextView preparationTime;

    public static Page4Fragment newInstance(Product product) {

        Page4Fragment p = new Page4Fragment();
        p.setProduct(product);
        return p;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.page4, container, false);

        ButterKnife.bind(this, view);

        Typeface titleTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/PathwayGothicOne-Regular.ttf");
        Typeface italicTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/FanwoodText-Italic.ttf");
        Typeface regularTypeface = Typeface.createFromAsset(this.getActivity().getAssets(), "fonts/FanwoodText-Regular.ttf");
        titlePage4.setTypeface(titleTypeface);
        price.setTypeface(regularTypeface);
        unitPage4.setTypeface(italicTypeface);
        preparationTimeTitle.setTypeface(regularTypeface);
        preparationTime.setTypeface(italicTypeface);

        titlePage4.setText(product.getTitle());
        price.setText(product.getPricePp() + " €");

        // declared in PageFragment
        setOrderButton();//defined in PageFragment

        return view;
    }
}

